import path from 'path'

import { IModule, } from '../types'

export const baseDir: string = path.resolve()

export function startModules (): void {
  async function start (_modules: IModule[]): Promise<void> {
    const modules: IModule[] = [ ..._modules, ]
    const module: IModule | undefined = modules.shift()

    if (!module) return

    await module.start()

    return start(modules)
  }
  
  const modules: IModule[] = Object.entries(getModules()).map(i => i[1])

  start(modules)
}

export function getModulesList (): string[] {
  return require(path.join(baseDir, 'crio-config')).modules
}

export function getModules (): object {
  const modules = {}

  getModulesList().map(moduleName => {
    modules[moduleName] = require(path.join(baseDir, 'src', 'server'))
  })

  return modules
}

