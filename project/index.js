"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.create = void 0;
var prompts_1 = __importDefault(require("prompts"));
var path_1 = __importDefault(require("path"));
var fs_extra_1 = __importDefault(require("fs-extra"));
var cli_1 = require("../cli");
var modules_1 = require("../modules");
var argv_1 = require("../argv");
function create() {
    return __awaiter(this, void 0, void 0, function () {
        var _a, projectName, modules, modulesResponses, projectDir, dependencies, devDependencies, packageJsonAddrs, crioConfigAddrs, copy, packages, packageJson, build;
        var _this = this;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    (0, cli_1.subTitle)('General Questions');
                    return [4 /*yield*/, (0, prompts_1.default)([
                            {
                                type: 'text',
                                name: 'projectName',
                                message: 'What will the project name be?',
                            },
                            {
                                type: 'multiselect',
                                name: 'modules',
                                message: 'What modules will be used in the project?',
                                choices: (0, modules_1.resumeModules)(),
                            },
                        ])];
                case 1:
                    _a = _b.sent(), projectName = _a.projectName, modules = _a.modules;
                    return [4 /*yield*/, modulesAsks(modules)];
                case 2:
                    modulesResponses = _b.sent();
                    projectDir = path_1.default.join(cli_1.baseDir, projectName);
                    dependencies = [
                        'nodemon',
                        'ts-node',
                        'typescript',
                        '@types/node',
                    ];
                    devDependencies = [
                        '@typescript-eslint/eslint-plugin',
                        '@typescript-eslint/parser eslint',
                        '@types/node',
                        'module-alias',
                        argv_1.options.dev ? cli_1.crioDir : '@crio/main/cli',
                    ];
                    packageJsonAddrs = path_1.default.join(projectDir, 'package.json');
                    crioConfigAddrs = path_1.default.join(cli_1.crioDir, 'temp/crio-config');
                    (0, cli_1.createDir)(projectDir);
                    (0, cli_1.createDir)(path_1.default.join(projectDir, 'src'));
                    return [4 /*yield*/, fs_extra_1.default.existsSync(crioConfigAddrs + '.ts')];
                case 3:
                    if (!_b.sent()) return [3 /*break*/, 5];
                    return [4 /*yield*/, fs_extra_1.default.unlinkSync(crioConfigAddrs + '.ts')];
                case 4:
                    _b.sent();
                    _b.label = 5;
                case 5: return [4 /*yield*/, fs_extra_1.default.existsSync(crioConfigAddrs + '.js')];
                case 6:
                    if (!_b.sent()) return [3 /*break*/, 8];
                    return [4 /*yield*/, fs_extra_1.default.unlinkSync(crioConfigAddrs + '.js')];
                case 7:
                    _b.sent();
                    _b.label = 8;
                case 8: return [4 /*yield*/, fs_extra_1.default.existsSync(crioConfigAddrs + '.js.map')];
                case 9:
                    if (!_b.sent()) return [3 /*break*/, 11];
                    return [4 /*yield*/, fs_extra_1.default.unlinkSync(crioConfigAddrs + '.js.map')];
                case 10:
                    _b.sent();
                    _b.label = 11;
                case 11: return [4 /*yield*/, fs_extra_1.default.writeFileSync(crioConfigAddrs + '.ts', '')];
                case 12:
                    _b.sent();
                    return [4 /*yield*/, installModules(projectName, modules, modulesResponses)];
                case 13:
                    _b.sent();
                    (0, cli_1.subTitle)('Applying final settings');
                    copy = {
                        message: 'Creating files...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, fs_extra_1.default.copySync(path_1.default.join(cli_1.crioDir, 'drafts', 'tsconfig.json'), path_1.default.join(projectDir, 'tsconfig.json'))];
                                    case 1:
                                        _a.sent();
                                        return [4 /*yield*/, fs_extra_1.default.copySync(path_1.default.join(cli_1.crioDir, 'drafts', 'index.ts'), path_1.default.join(projectDir, 'src', 'index.ts'))];
                                    case 2:
                                        _a.sent();
                                        return [4 /*yield*/, fs_extra_1.default.copySync(path_1.default.join(cli_1.crioDir, 'temp', 'crio-config.ts'), path_1.default.join(projectDir, 'crio-config.ts'))];
                                    case 3:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    packages = {
                        message: 'Installing core dependencies...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, (0, cli_1.exec)("npm install --save ".concat(dependencies.join(' ')), projectDir)];
                                    case 1:
                                        _a.sent();
                                        return [4 /*yield*/, (0, cli_1.exec)("npm install --save-dev ".concat(devDependencies.join(' ')), projectDir)];
                                    case 2:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    packageJson = {
                        message: 'Adjusting package.json...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            var packageJson;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        packageJson = require(packageJsonAddrs);
                                        packageJson.name = projectName;
                                        packageJson.version = '1.0.0';
                                        packageJson.description = '';
                                        packageJson.scripts = packageJson.scripts || {};
                                        packageJson.scripts = {
                                            'build': 'tsc',
                                            'build:watch': 'tsc --watch',
                                            'start': 'nodemon ./src',
                                            'start:watch': 'nodemon ./src --watch',
                                            'start:dev': 'ts-node src/index.ts',
                                            'start:dev:watch': 'ts-node src/index.ts --watch',
                                        };
                                        return [4 /*yield*/, fs_extra_1.default.writeFileSync(packageJsonAddrs, (0, cli_1.objectToString)(packageJson))];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    build = {
                        message: 'Building...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, (0, cli_1.exec)('npm run build', projectDir)];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    return [4 /*yield*/, (0, cli_1.progress)([copy, packages, packageJson, build,], 'Done!')];
                case 14:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    });
}
exports.create = create;
function modulesAsks(_modules) {
    return __awaiter(this, void 0, void 0, function () {
        var responses, modules, module, Module, _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    responses = {};
                    modules = __spreadArray([], _modules, true);
                    module = modules.shift();
                    if (!module)
                        return [2 /*return*/, {}];
                    Module = require(path_1.default.join(module, 'cli'));
                    (0, cli_1.subTitle)("".concat(Module.title, " Questions"));
                    _a = responses;
                    _b = module;
                    return [4 /*yield*/, Module.create()];
                case 1:
                    _a[_b] = _c.sent();
                    return [2 /*return*/, modules.length === 0 ? responses : __assign(__assign({}, responses), modulesAsks(modules))];
            }
        });
    });
}
function installModules(projectName, _modules, modulesResponses) {
    return __awaiter(this, void 0, void 0, function () {
        var modules, module, Module, prepare;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    modules = __spreadArray([], _modules, true);
                    module = modules.shift();
                    if (!module)
                        return [2 /*return*/];
                    Module = require(path_1.default.join(module, 'cli'));
                    (0, cli_1.subTitle)("Installing ".concat(Module.title, "..."));
                    return [4 /*yield*/, Module.prepareInstall({
                            projectName: projectName,
                            responses: modulesResponses[module],
                        })];
                case 1:
                    prepare = _a.sent();
                    return [4 /*yield*/, installModule(prepare)];
                case 2:
                    _a.sent();
                    if (!(modules.length > 0)) return [3 /*break*/, 4];
                    return [4 /*yield*/, installModules(projectName, modules, modulesResponses)];
                case 3:
                    _a.sent();
                    _a.label = 4;
                case 4: return [2 /*return*/];
            }
        });
    });
}
function installModule(prepare) {
    return __awaiter(this, void 0, void 0, function () {
        var srcDir, cp, mkdir, config, npmInstall, npmLink, npmInstallG;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    srcDir = path_1.default.join(prepare.projectDir, 'src');
                    cp = {
                        message: 'Copying files...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            function handler(filesList) {
                                if (filesList === void 0) { filesList = prepare.copy || []; }
                                return __awaiter(this, void 0, void 0, function () {
                                    var files, file, origin, destiny;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                if (filesList.length === 0)
                                                    return [2 /*return*/];
                                                files = __spreadArray([], filesList, true);
                                                file = files.shift();
                                                origin = path_1.default.join(prepare.moduleDir, 'drafts', file[0]);
                                                destiny = path_1.default.join(srcDir, file[1]);
                                                return [4 /*yield*/, fs_extra_1.default.copySync(origin, destiny)];
                                            case 1:
                                                _a.sent();
                                                return [4 /*yield*/, handler(files)];
                                            case 2:
                                                _a.sent();
                                                return [2 /*return*/];
                                        }
                                    });
                                });
                            }
                            return __generator(this, function (_a) {
                                handler();
                                return [2 /*return*/];
                            });
                        }); },
                    };
                    mkdir = {
                        message: 'Creating directory...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a;
                            return __generator(this, function (_b) {
                                prepare.createDir = ((_a = prepare.createDir) === null || _a === void 0 ? void 0 : _a.map(function (dir) {
                                    return path_1.default.join(srcDir, dir);
                                })) || [];
                                if (!(0, cli_1.createDir)(prepare.createDir, true)) {
                                    process.exit();
                                }
                                return [2 /*return*/];
                            });
                        }); },
                    };
                    config = {
                        message: 'Adding settings...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            var configAddrs, Config, content;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        configAddrs = path_1.default.join(cli_1.crioDir, 'temp', 'crio-config.ts');
                                        Config = __assign(__assign({ modules: [] }, require(configAddrs)), prepare.config);
                                        content = [];
                                        Config.modules = __spreadArray(__spreadArray([], Config.modules, true), [prepare.moduleName,], false);
                                        Object.entries(Config).forEach(function (_a) {
                                            var key = _a[0], value = _a[1];
                                            if (typeof value === 'string') {
                                                value = "'".concat(value, "'");
                                            }
                                            else if (typeof value === 'object') {
                                                value = (0, cli_1.objectToString)(value);
                                            }
                                            content.push("export const ".concat(key, " = ").concat(value));
                                        });
                                        return [4 /*yield*/, fs_extra_1.default.appendFileSync(configAddrs, content.join('\n'))];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    npmInstall = {
                        message: 'Installing dependencies...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, _b;
                            return __generator(this, function (_c) {
                                switch (_c.label) {
                                    case 0:
                                        if (((_a = prepare.dependencies) === null || _a === void 0 ? void 0 : _a.length) === 0)
                                            return [2 /*return*/];
                                        return [4 /*yield*/, (0, cli_1.exec)("npm install --save ".concat((_b = prepare.dependencies) === null || _b === void 0 ? void 0 : _b.join(' ')), prepare.projectDir)];
                                    case 1:
                                        _c.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    npmLink = {
                        message: 'Installing linked dependencies...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, _b;
                            return __generator(this, function (_c) {
                                switch (_c.label) {
                                    case 0:
                                        if (((_a = prepare.linkDependencies) === null || _a === void 0 ? void 0 : _a.length) === 0)
                                            return [2 /*return*/];
                                        return [4 /*yield*/, (0, cli_1.exec)("npm link ".concat((_b = prepare.linkDependencies) === null || _b === void 0 ? void 0 : _b.join(' ')), prepare.projectDir)];
                                    case 1:
                                        _c.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    npmInstallG = {
                        message: 'Installing global dependencies...',
                        handler: function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, _b;
                            return __generator(this, function (_c) {
                                switch (_c.label) {
                                    case 0:
                                        if (((_a = prepare.globalDependencies) === null || _a === void 0 ? void 0 : _a.length) === 0)
                                            return [2 /*return*/];
                                        return [4 /*yield*/, (0, cli_1.exec)("npm install -g ".concat((_b = prepare.globalDependencies) === null || _b === void 0 ? void 0 : _b.join(' ')), prepare.projectDir)];
                                    case 1:
                                        _c.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); },
                    };
                    return [4 /*yield*/, (0, cli_1.progress)([cp, mkdir, config, npmInstall, npmLink, npmInstallG,], 'Done!')];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
//# sourceMappingURL=index.js.map