"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calcLimitOffset = void 0;
var calcLimitOffset = function (page, perpage) {
    page = page || 0;
    perpage = perpage || 10;
    var limit = perpage;
    var offset = (page * perpage) - perpage;
    return { limit: limit, offset: offset >= 0 ? offset : 0, };
};
exports.calcLimitOffset = calcLimitOffset;
//# sourceMappingURL=index.js.map