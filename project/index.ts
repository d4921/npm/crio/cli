import prompts from 'prompts'
import path from 'path'
import fs from 'fs-extra'

import { IBasicCreateProjectResponse, IModuleCli, IModuleCliConfig, IProgressStep, } from '../types'

import { subTitle, baseDir, createDir, exec, progress, objectToString, crioDir, } from '../cli'
import { resumeModules, } from '../modules'
import { options, } from '../argv'

export async function create (): Promise<void> {
  subTitle('General Questions')

  const { projectName, modules, }: IBasicCreateProjectResponse = await prompts([
    {
      type: 'text',
      name: 'projectName',
      message: 'What will the project name be?',
    },
    {
      type: 'multiselect',
      name: 'modules',
      message: 'What modules will be used in the project?',
      choices: resumeModules(),
    },
  ])

  
  const modulesResponses: object = await modulesAsks(modules)
  const projectDir = path.join(baseDir, projectName)

  const dependencies: string[] = [
    'nodemon',
    'ts-node',
    'typescript',
    '@types/node',
  ]

  const devDependencies: string[] = [
    '@typescript-eslint/eslint-plugin',
    '@typescript-eslint/parser eslint',
    '@types/node',
    'module-alias',
    options.dev ? crioDir : '@crio/main/cli',
  ]

  // subTitle('Applying initial settings')

  const packageJsonAddrs: string = path.join(projectDir, 'package.json')
  const crioConfigAddrs = path.join(crioDir, 'temp/crio-config')

  createDir(projectDir)
  createDir(path.join(projectDir, 'src'))

  if (await fs.existsSync(crioConfigAddrs + '.ts')) {
    await fs.unlinkSync(crioConfigAddrs + '.ts')
  }

  if (await fs.existsSync(crioConfigAddrs + '.js')) {
    await fs.unlinkSync(crioConfigAddrs + '.js')
  }

  if (await fs.existsSync(crioConfigAddrs + '.js.map')) {
    await fs.unlinkSync(crioConfigAddrs + '.js.map')
  }

  await fs.writeFileSync(crioConfigAddrs + '.ts', '')
  await installModules(projectName, modules, modulesResponses)

  subTitle('Applying final settings')

  const copy: IProgressStep = {
    message: 'Creating files...',
    handler: async (): Promise<void> => {
      await fs.copySync(path.join(crioDir, 'drafts', 'tsconfig.json'), path.join(projectDir, 'tsconfig.json'))
      await fs.copySync(path.join(crioDir, 'drafts', 'index.ts'), path.join(projectDir, 'src', 'index.ts'))
      await fs.copySync(path.join(crioDir, 'temp', 'crio-config.ts'), path.join(projectDir, 'crio-config.ts'))
    },
  }

  const packages: IProgressStep = {
    message: 'Installing core dependencies...',
    handler: async (): Promise<void> => {
      await exec(`npm install --save ${dependencies.join(' ')}`, projectDir)
      await exec(`npm install --save-dev ${devDependencies.join(' ')}`, projectDir)
    },
  }

  const packageJson: IProgressStep = {
    message: 'Adjusting package.json...',
    handler: async (): Promise<void> => {
      const packageJson: any = require(packageJsonAddrs)

      packageJson.name = projectName
      packageJson.version = '1.0.0'
      packageJson.description = ''
      packageJson.scripts = packageJson.scripts || {}
      packageJson.scripts = {
        'build': 'tsc',
        'build:watch': 'tsc --watch',
        'start': 'nodemon ./src',
        'start:watch': 'nodemon ./src --watch',
        'start:dev': 'ts-node src/index.ts',
        'start:dev:watch': 'ts-node src/index.ts --watch',
      }

      await fs.writeFileSync(packageJsonAddrs, objectToString(packageJson))
    },
  }

  const build: IProgressStep = {
    message: 'Building...',
    handler: async (): Promise<void> => {
      await exec('npm run build', projectDir)
    },
  }

  await progress([ copy, packages, packageJson, build, ], 'Done!')
}

async function modulesAsks (_modules: string[]): Promise<object> {
  const responses = {}
  const modules = [ ..._modules, ]
  const module = modules.shift()

  if (!module) return {}

  const Module: IModuleCli = require(path.join(module, 'cli'))

  subTitle(`${Module.title} Questions`)

  responses[module] = await Module.create()

  return modules.length === 0 ? responses : { ...responses, ...modulesAsks(modules), }
}

async function installModules (projectName: string, _modules: string[], modulesResponses: object): Promise<void> {
  const modules = [ ..._modules, ]
  const module = modules.shift()

  if (!module) return

  const Module: IModuleCli = require(path.join(module, 'cli'))

  subTitle(`Installing ${Module.title}...`)

  const prepare = await Module.prepareInstall({
    projectName,
    responses: modulesResponses[module],
  })

  await installModule(prepare)

  if (modules.length > 0) {
    await installModules(projectName, modules, modulesResponses)
  } 
}

async function installModule (prepare: IModuleCliConfig): Promise<void> {
  const srcDir = path.join(prepare.projectDir, 'src')

  const cp: IProgressStep = {
    message: 'Copying files...',
    handler: async (): Promise<void> => {
      async function handler (filesList: [string, string][] = prepare.copy || []): Promise<void> {
        if (filesList.length === 0) return
        const files: [string, string][] = [ ...filesList, ]
        const file: [string, string] = files.shift() as [string, string]
        
        const origin = path.join(prepare.moduleDir, 'drafts', file[0])
        const destiny = path.join(srcDir, file[1])
        
        await fs.copySync(origin, destiny)
        await handler(files)
      }

      handler()
    },
  }

  const mkdir: IProgressStep = {
    message: 'Creating directory...',
    handler: async (): Promise<void> => {
      prepare.createDir = prepare.createDir?.map((dir: string): string => {
        return path.join(srcDir, dir)
      }) || []

      if (!createDir(prepare.createDir, true)) {
        process.exit()
      }
    },
  }

  const config: IProgressStep = {
    message: 'Adding settings...',
    handler: async (): Promise<void> => {
      const configAddrs: string = path.join(crioDir, 'temp', 'crio-config.ts')
      const Config = { modules: [], ...require(configAddrs), ...prepare.config, }
      const content: string[] = []

      Config.modules = [ ...Config.modules, prepare.moduleName, ]
      
      Object.entries(Config).forEach(([ key, value, ]: [ string, any, ]) => {
        if (typeof value === 'string') {
          value = `'${value}'`
        }
        else if (typeof value === 'object') {
          value = objectToString(value)
        }

        content.push(`export const ${key} = ${value}`)
      })

      await fs.appendFileSync(configAddrs, content.join('\n'))
    },
  }

  const npmInstall: IProgressStep = {
    message: 'Installing dependencies...',
    handler: async (): Promise<void> => {
      if (prepare.dependencies?.length === 0) return

      await exec(`npm install --save ${prepare.dependencies?.join(' ')}`, prepare.projectDir )
    },
  }

  const npmLink: IProgressStep = {
    message: 'Installing linked dependencies...',
    handler: async (): Promise<void> => {
      if (prepare.linkDependencies?.length === 0) return

      await exec(`npm link ${prepare.linkDependencies?.join(' ')}`, prepare.projectDir )
    },
  }

  const npmInstallG: IProgressStep = {
    message: 'Installing global dependencies...',
    handler: async (): Promise<void> => {
      if (prepare.globalDependencies?.length === 0) return

      await exec(`npm install -g ${prepare.globalDependencies?.join(' ')}`, prepare.projectDir )
    },
  }

  await progress([ cp, mkdir, config, npmInstall, npmLink, npmInstallG, ], 'Done!')
}