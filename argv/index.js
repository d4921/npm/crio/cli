"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.targets = exports.options = exports.args = void 0;
var argv_1 = __importDefault(require("argv"));
argv_1.default.clear().option([
    {
        name: 'dev',
        short: 'd',
        type: 'boolean',
        description: 'Sets run to development mode (Crio developers only)',
        example: "'crio -d",
    },
]);
exports.args = argv_1.default.run();
exports.options = exports.args.options;
exports.targets = exports.args.targets;
//# sourceMappingURL=index.js.map